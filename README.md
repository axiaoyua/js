# JS-博客项目


## 1.
首先，你的电脑必须下载了node
再npm i

## 2.
初始化数据库：
把tools目录下的datatest.js的最后几行代码解除注释
然后初始化数据：
cd tools
node datatest.js
在执行完后要把那一串代码注释掉

## 3.
运行服务器：
输入node server.js
在浏览器输入http://localhost:9999

## 登录页账号：admin

## 密码：123456
