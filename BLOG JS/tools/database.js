const mongodb = require('mongodb')
const MongoClient = mongodb.MongoClient
const mongodbUrl = 'mongodb://localhost:27017'
const ObjectId = mongodb.ObjectId

var DataBase = function (kuName, biaoName) {
    this.kuName = kuName
    this.biaoName = biaoName

    // 插入一条数据
    this.insertOneData = function (newDataObj, callback) {
        // 数据库的连接
        MongoClient.connect(mongodbUrl, (err, client) => {
            if (err) {
                console.log('数据在连接的时候出现了问题', err)
            } else {
                // 选库
                // console.log(this.kuName)
                var dbName = client.db(this.kuName)
                // 选表
                var collName = dbName.collection(this.biaoName)
                // 插入一条数据
                collName.insertOne(newDataObj, (error, final) => {
                    if (error) {
                        console.log('数据库在插入一条数据的时候出现了问题', error)
                    } else {
                        if (callback) {
                            callback(err,error,final)
                        } else {
                            if (final.acknowledged) {
                                console.log('数据插入成功')
                            } else {
                                console.log('数据插入失败')
                            }
                        }
                        // callback(final)

                        // 关闭数据库
                        client.close()
                    }
                })
            }
        })
    }

    // 插入多条数据
    this.insertManyData = function (newDataObjArr, callback) {
        MongoClient.connect(mongodbUrl, (err, client) => {
            if (err) {
                console.log('数据在连接的时候出现了问题', err)
            } else {
                var dbName = client.db(this.kuName)
                var collName = dbName.collection(this.biaoName)
                collName.insertMany(newDataObjArr, (error, final) => {
                    if (error) {
                        console.log('数据库在插入多条数据的时候出现了问题', error)
                    } else {
                        if (callback) {
                            callback(err,error,final)
                        } else {
                            if (final.acknowledged) {
                                console.log('数据插入成功')
                            } else {
                                console.log('数据插入失败')
                            }
                        }
                        client.close()
                    }
                })
            }
        })
    }

    // 删除一条数据
    this.deleteOneData = function (conditionObj, callback) {
        MongoClient.connect(mongodbUrl, (err, client) => {
            if (err) {
                console.log('数据在连接的时候出现了问题', err)
            } else {
                var dbName = client.db(this.kuName)
                var collName = dbName.collection(this.biaoName)
                collName.deleteOne(conditionObj, (error, final) => {
                    if (error) {
                        console.log('数据库在删除一条数据的时候出现了问题', error)
                    } else {
                        if (callback) {
                            callback(err,error,final)
                        } else {
                            if (final.acknowledged) {
                                console.log('数据删除成功')
                            } else {
                                console.log('数据删除失败')
                            }
                        }
                        client.close()
                    }
                })
            }
        })
    }

    // 删除多条数据
    this.deleteManyData = function (conditionObj, callback) {
        MongoClient.connect(mongodbUrl, (err, client) => {
            if (err) {
                console.log('数据在连接的时候出现了问题', err)
            } else {
                var dbName = client.db(this.kuName)
                var collName = dbName.collection(this.biaoName)
                collName.deleteMany(conditionObj, (error, final) => {
                    if (error) {
                        console.log('数据库在删除多条数据的时候出现了问题', error)
                    } else {
                        if (callback) {
                            callback(err,error,final)
                        } else {
                            if (final.acknowledged) {
                                console.log('数据删除成功')
                            } else {
                                console.log('数据删除失败')
                            }
                        }
                        client.close()
                    }
                })
            }
        })
    }

    // 根据_id来精准删除数据
    this.deleteOneDataById = function (conditonId, callback) {
        // 根据传进来的id来组成一个对象
        var idObj = { _id: ObjectId(conditonId) }
        this.deleteOneData(idObj, callback)
    }

    // 查询数据
    this.findData = function (conditionObj, callback) {
        MongoClient.connect(mongodbUrl, (err, client) => {
            if (err) {
                console.log('数据在连接的时候出现了问题', err)
            } else {
                var dbName = client.db(this.kuName)
                var collName = dbName.collection(this.biaoName)
                collName.find(conditionObj).toArray((error,final)=>{
                    if(error){
                        console.log('数据库在查询数据的时候出现了问题',error)
                    }else{
                        if(callback){
                            callback(err,error,final)
                        }else{
                            console.log('这是你需要查询的数据\n',final)
                        }
                        client.close()
                    }

                })
            }
        })
    }

    // 根据id精准查询
    this.findDataById=function(conditonId, callback){
        var idObj={_id:ObjectId(conditonId)}
        this.findData(idObj,callback)
    }

    // 更新/修改一条数据
    this.updateOneData=function(conditionObj,newObj,callback){
        MongoClient.connect(mongodbUrl, (err, client) => {
            if (err) {
                console.log('数据在连接的时候出现了问题', err)
            } else {
                var dbName = client.db(this.kuName)
                var collName = dbName.collection(this.biaoName)
                collName.updateOne(conditionObj,{$set:newObj}, (error, final) => {
                    if (error) {
                        console.log('数据库在更新/修改一条数据的时候出现了问题', error)
                    } else {
                        if (callback) {
                            callback(err,error,final)
                        } else {
                            if (final.acknowledged) {
                                console.log('数据更新/修改成功')
                            } else {
                                console.log('数据更新/修改失败')
                            }
                        }
                        client.close()
                    }
                })
            }
        })
    }

    // 更新/修改多条数据
    this.updateManyData=function(conditionObj,newObj,callback){
        MongoClient.connect(mongodbUrl, (err, client) => {
            if (err) {
                console.log('数据在连接的时候出现了问题', err)
            } else {
                var dbName = client.db(this.kuName)
                var collName = dbName.collection(this.biaoName)
                collName.updateMany(conditionObj,{$set:newObj}, (error, final) => {
                    if (error) {
                        console.log('数据库在更新/修改多条数据的时候出现了问题', error)
                    } else {
                        if (callback) {
                            callback(err,error,final)
                        } else {
                            if (final.acknowledged) {
                                console.log('数据更新/修改成功')
                            } else {
                                console.log('数据更新/修改失败')
                            }
                        }
                        client.close()
                    }
                })
            }
        })
    }

    // 根据id精准更新数据
    this.updateOneDataById=function(conditonId,newObj,callback){
        var idObj={_id:ObjectId(conditonId)}
        this.updateOneData(idObj,newObj,callback)
    }

}

// 引出的是函数
module.exports=DataBase