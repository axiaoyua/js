const DataBase = require('./database')

var Articles = new DataBase('BLOG', 'wenzhang')
var Comments = new DataBase('BLOG', 'pinglun')

var jsArticles = new DataBase('BLOG', 'js')
var nodeJSArticles = new DataBase('BLOG', 'nodejs')
var httpArticles = new DataBase('BLOG', 'http')
var ajaxArticles = new DataBase('BLOG', 'ajax')
var expressArticles = new DataBase('BLOG', 'express')
var mongodbArticles = new DataBase('BLOG', 'mongo')
var jqueryArticles = new DataBase('BLOG', 'jq')
var reactArticles = new DataBase('BLOG', 'react')
var vueArticles = new DataBase('BLOG', 'vue')

var authorCenter=new DataBase('BLOG','myself')


var wenZhangObjArr = [
    {
        title: ' 科技爱好者周刊（第 179 期）：AR 技术的打开方式',
        time: '2021年10月15日',
        author: '阮一峰',
        content: `游客可以探索这些虚拟对象，也可以根据提示，完成指定任务，解锁关卡。新闻说，这个公园一共部署了35个 AR 应用，其中13个是免费作品，其余22个必须付费4.99欧元才能激活，相当于公园门票了。我觉得，这个主意太好了，值得借鉴。城市公园改建为 AR 游戏场，这才是 AR 技术的正确打开方式。 公园还是那个公园，什么都没变，但是加入了电子游戏的成分。这里最关键的一点是，AR 就是应该在室外玩。仔细想一下，你会发现，绝大多数电子游戏只适合在室内玩，但是 AR 可以在室外玩，而且室外比室内好玩得多。这是因为 AR 是部分虚拟、部分现实，虚拟层需要叠加在现实层之上，如果现实空间太小、太单调，就不可能设计出好玩的 AR 应用。目前，国内影响最大的 AR 应用，就是支付宝"集五福"的室内找"福"字，这个游戏非常乏味，原因就在这里，室内很难想出好玩的玩法。`
    },
    {
        title: '科技爱好者周刊（第 178 期）：家庭太阳能发电的春天',
        time: '2021年10月 8日 ',
        author: '阮一峰',
        content: `但是，电价没涨，火力发电厂因此陷入亏损，每发一度电都会亏钱。偏偏又遇上用电量猛增，今年前八个月，全国用电量增加了13.8%，相比之下，这个数字去年是3.1%，前年是4.5%。用电量增长太快，电厂又无力多发电，所以电就不够了。有的同学可能会说，火电不行，不是还有风电和光电吗？问题是它们只是一个零头，不到全部发电量的10%，根本替代不了火电，火电是绝对的大头，占到70%以上。而且，风电和光电不稳定，英国的北海今年突然风停了，风电急剧减少，英国的能源就紧张到90%的加油站没有汽油了。`
    },
    {
        title: 'JavaScript 侦测手机浏览器的五种方法',
        time: '2021年09月29日',
        author: '阮一峰',
        content: `上面示例中，CSS 语句pointer:coarse表示当前设备的指针是不精确的。由于手机不支持鼠标，只支持触摸，所以符合这个条件。有些设备支持多种指针，比如同时支持鼠标和触摸。pointer:coarse只用来判断主指针，此外还有一个any-pointer命令判断所有指针。`
    },
    {
        title: '科技爱好者周刊（第 177 期）：iPad 的真正用途',
        time: '2021年09月24日',
        author: '阮一峰',
        content: `如果有人说，平板电脑是生产力工具，那属于胡说。无论是学习和工作，它远远不如笔记本电脑好用和强大。我曾经幻想，外出时使用平板电脑开发。结果发现根本不可行，要什么缺什么，开发体验极差，最多只能紧急时登录服务器，或者偶尔改一下仓库代码。但是，我最近发现有一个用途，平板电脑大大强于笔记本，几乎可以说，是为这种用途量身定制的工具。那就是普通人的视频处理。`
    },
]

var pingLunObjArr = [
    {
        articleId: 1111,
        commentContent: 111111,
        commentAuthor: 111,
        commandTime: 111,
        state: 0
        // state：用来控制评论是否展示。初始state：0
        // 1，展示。2，不展示

    }
]

var jsObjArr = [
    {
        title: `王衍华如饥似渴，张红缨且惊且娇【一】`,
        time: `2020.06.21 11:22:13`,
        author: `南山顽石`,
        content: `衍平、衍举赶到中心小学时，红缨的房子恰好亮着灯，且从窗户传出阵阵嘻嘻哈哈的说话声，甚是热闹。敲开门时，却见除了红缨外，汪衍华、孙老师也在，每个人手里都拿着一把牌，原来他们在玩弥竹竿。衍举看了衍华一眼，嘿嘿一笑说：“衍华，今儿黑跟你媳妇同房呀？”张红缨胀得满面通红，但知道汪衍举跟谁说话都是这个样子，没轻没重的，她便不好怪病他，只得讪笑着说：“哪呢！等给孙校长把房子收拾好，天已经不早了。所以衍华就不回去了，今儿黑他在孙校长那儿歇。衍举哥也真是的！我跟衍华都是规规矩矩的老实疙瘩子，你把我俩当啥人了呢？”衍举道：“我说得耍呢。”衍平道：“红缨，我们也是无事不登三宝殿，是有个事想叫你帮忙。”接着便前拉后扯的说了何秀莲叫水吹走了，他们沿河寻尸首，以及汪耀林叫蛇咬了，被送到县医院去了等语，最后方说想请她去耀臣那儿帮忙做饭。听到何秀莲的死讯，红缨、孙老师和王衍华均极为吃惊。红缨道：“早上到河边送孙校长时，还活蹦乱跳的一个人，咋就叫水吹去了呢？秀莲实在是命苦……”说着说着眼睛就湿了，急忙掏出手绢擦了擦眼角。孙老师皱眉沉思片刻说：“俗话说活要见人，死要见尸。现在啥都没找到，就不要急着下结论。你们随后再找几天再说吧。明天一早，我再去公社把情况说一下，叫安排广播站广播一个寻人启事，把大家都发动起来，不管是找到活人，退一步，就算是尸首，人多了，找到的希望总归大一些。”作者：南山顽石链接：https://www.jianshu.com/p/fd0903fdd408来源：简书著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。`,
        good: 0,
        bad: 0,
        comment: 0,
    },
    {
        title: `王衍华如饥似渴，张红缨且惊且娇【二】`,
        time: `2020.06.21 11:22:13`,
        author: `南山顽石`,
        content: `衍平、衍举赶到中心小学时，红缨的房子恰好亮着灯，且从窗户传出阵阵嘻嘻哈哈的说话声，甚是热闹。敲开门时，却见除了红缨外，汪衍华、孙老师也在，每个人手里都拿着一把牌，原来他们在玩弥竹竿。衍举看了衍华一眼，嘿嘿一笑说：“衍华，今儿黑跟你媳妇同房呀？”张红缨胀得满面通红，但知道汪衍举跟谁说话都是这个样子，没轻没重的，她便不好怪病他，只得讪笑着说：“哪呢！等给孙校长把房子收拾好，天已经不早了。所以衍华就不回去了，今儿黑他在孙校长那儿歇。衍举哥也真是的！我跟衍华都是规规矩矩的老实疙瘩子，你把我俩当啥人了呢？”衍举道：“我说得耍呢。”衍平道：“红缨，我们也是无事不登三宝殿，是有个事想叫你帮忙。”接着便前拉后扯的说了何秀莲叫水吹走了，他们沿河寻尸首，以及汪耀林叫蛇咬了，被送到县医院去了等语，最后方说想请她去耀臣那儿帮忙做饭。听到何秀莲的死讯，红缨、孙老师和王衍华均极为吃惊。红缨道：“早上到河边送孙校长时，还活蹦乱跳的一个人，咋就叫水吹去了呢？秀莲实在是命苦……”说着说着眼睛就湿了，急忙掏出手绢擦了擦眼角。孙老师皱眉沉思片刻说：“俗话说活要见人，死要见尸。现在啥都没找到，就不要急着下结论。你们随后再找几天再说吧。明天一早，我再去公社把情况说一下，叫安排广播站广播一个寻人启事，把大家都发动起来，不管是找到活人，退一步，就算是尸首，人多了，找到的希望总归大一些。”作者：南山顽石链接：https://www.jianshu.com/p/fd0903fdd408来源：简书著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。`,
        good: 0,
        bad: 0,
        comment: 0,
    },
    {
        title: `王衍华如饥似渴，张红缨且惊且娇【三】`,
        time: `2020.06.21 11:22:13`,
        author: `南山顽石`,
        content: `衍平、衍举赶到中心小学时，红缨的房子恰好亮着灯，且从窗户传出阵阵嘻嘻哈哈的说话声，甚是热闹。敲开门时，却见除了红缨外，汪衍华、孙老师也在，每个人手里都拿着一把牌，原来他们在玩弥竹竿。衍举看了衍华一眼，嘿嘿一笑说：“衍华，今儿黑跟你媳妇同房呀？”张红缨胀得满面通红，但知道汪衍举跟谁说话都是这个样子，没轻没重的，她便不好怪病他，只得讪笑着说：“哪呢！等给孙校长把房子收拾好，天已经不早了。所以衍华就不回去了，今儿黑他在孙校长那儿歇。衍举哥也真是的！我跟衍华都是规规矩矩的老实疙瘩子，你把我俩当啥人了呢？”衍举道：“我说得耍呢。”衍平道：“红缨，我们也是无事不登三宝殿，是有个事想叫你帮忙。”接着便前拉后扯的说了何秀莲叫水吹走了，他们沿河寻尸首，以及汪耀林叫蛇咬了，被送到县医院去了等语，最后方说想请她去耀臣那儿帮忙做饭。听到何秀莲的死讯，红缨、孙老师和王衍华均极为吃惊。红缨道：“早上到河边送孙校长时，还活蹦乱跳的一个人，咋就叫水吹去了呢？秀莲实在是命苦……”说着说着眼睛就湿了，急忙掏出手绢擦了擦眼角。孙老师皱眉沉思片刻说：“俗话说活要见人，死要见尸。现在啥都没找到，就不要急着下结论。你们随后再找几天再说吧。明天一早，我再去公社把情况说一下，叫安排广播站广播一个寻人启事，把大家都发动起来，不管是找到活人，退一步，就算是尸首，人多了，找到的希望总归大一些。”作者：南山顽石链接：https://www.jianshu.com/p/fd0903fdd408来源：简书著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。`,
        good: 0,
        bad: 0,
        comment: 0,
    },
    {
        title: `王衍华如饥似渴，张红缨且惊且娇【四】`,
        time: `2020.06.21 11:22:13`,
        author: `南山顽石`,
        content: `衍平、衍举赶到中心小学时，红缨的房子恰好亮着灯，且从窗户传出阵阵嘻嘻哈哈的说话声，甚是热闹。敲开门时，却见除了红缨外，汪衍华、孙老师也在，每个人手里都拿着一把牌，原来他们在玩弥竹竿。衍举看了衍华一眼，嘿嘿一笑说：“衍华，今儿黑跟你媳妇同房呀？”张红缨胀得满面通红，但知道汪衍举跟谁说话都是这个样子，没轻没重的，她便不好怪病他，只得讪笑着说：“哪呢！等给孙校长把房子收拾好，天已经不早了。所以衍华就不回去了，今儿黑他在孙校长那儿歇。衍举哥也真是的！我跟衍华都是规规矩矩的老实疙瘩子，你把我俩当啥人了呢？”衍举道：“我说得耍呢。”衍平道：“红缨，我们也是无事不登三宝殿，是有个事想叫你帮忙。”接着便前拉后扯的说了何秀莲叫水吹走了，他们沿河寻尸首，以及汪耀林叫蛇咬了，被送到县医院去了等语，最后方说想请她去耀臣那儿帮忙做饭。听到何秀莲的死讯，红缨、孙老师和王衍华均极为吃惊。红缨道：“早上到河边送孙校长时，还活蹦乱跳的一个人，咋就叫水吹去了呢？秀莲实在是命苦……”说着说着眼睛就湿了，急忙掏出手绢擦了擦眼角。孙老师皱眉沉思片刻说：“俗话说活要见人，死要见尸。现在啥都没找到，就不要急着下结论。你们随后再找几天再说吧。明天一早，我再去公社把情况说一下，叫安排广播站广播一个寻人启事，把大家都发动起来，不管是找到活人，退一步，就算是尸首，人多了，找到的希望总归大一些。”`,
        good: 0,
        bad: 0,
        comment: 0,
    },
    {
        title: `王衍华如饥似渴，张红缨且惊且娇【五】`,
        time: `2020.06.21 11:22:13`,
        author: `南山顽石`,
        content: `衍平、衍举赶到中心小学时，红缨的房子恰好亮着灯，且从窗户传出阵阵嘻嘻哈哈的说话声，甚是热闹。敲开门时，却见除了红缨外，汪衍华、孙老师也在，每个人手里都拿着一把牌，原来他们在玩弥竹竿。衍举看了衍华一眼，嘿嘿一笑说：“衍华，今儿黑跟你媳妇同房呀？”张红缨胀得满面通红，但知道汪衍举跟谁说话都是这个样子，没轻没重的，她便不好怪病他，只得讪笑着说：“哪呢！等给孙校长把房子收拾好，天已经不早了。所以衍华就不回去了，今儿黑他在孙校长那儿歇。衍举哥也真是的！我跟衍华都是规规矩矩的老实疙瘩子，你把我俩当啥人了呢？”衍举道：“我说得耍呢。”衍平道：“红缨，我们也是无事不登三宝殿，是有个事想叫你帮忙。”接着便前拉后扯的说了何秀莲叫水吹走了，他们沿河寻尸首，以及汪耀林叫蛇咬了，被送到县医院去了等语，最后方说想请她去耀臣那儿帮忙做饭。听到何秀莲的死讯，红缨、孙老师和王衍华均极为吃惊。红缨道：“早上到河边送孙校长时，还活蹦乱跳的一个人，咋就叫水吹去了呢？秀莲实在是命苦……”说着说着眼睛就湿了，急忙掏出手绢擦了擦眼角。孙老师皱眉沉思片刻说：“俗话说活要见人，死要见尸。现在啥都没找到，就不要急着下结论。你们随后再找几天再说吧。明天一早，我再去公社把情况说一下，叫安排广播站广播一个寻人启事，把大家都发动起来，不管是找到活人，退一步，就算是尸首，人多了，找到的希望总归大一些。”`,
        good: 0,
        bad: 0,
        comment: 0,
    }
]
var nodejsObjArr = [
    {
        title: `秦乐园【一】`,
        time: `2020.06.21 11:22:13`,
        author: `南山顽石`,
        content: `一个月后，我终于又回到了临淄，除了以长辈的名义召开了宗室会议，把宗主的位置传给了曦儿，没有公开露过面。反正只要有高权限的令牌，在哪里都通行无阻。

        再好的人皮面具，一天也只能戴几个时辰。所以我白天基本就是在房里学琴、对弈。闷久了就去看排舞。我甚至还作了一首歌，只是曲意悲怆，音调极高，不是能传于市井的曲子。
        
        回来这么久，一直没有流枫的消息。我的意思是想他回哥哥那里，或者去爹爹那里也不错。当然，他本来就是四叔从杀手里简拔出来的，要是想继续做杀手，也不失为正途。
        
        转眼就到了七月半。我现在也算是半人半鬼，到了清明和鬼节，总要享受一下反客为主的乐趣。
        
        我戴上人皮面具，拎了酒，找了个观景位置不错的高台，一边等着月亮出来一边自斟自饮。
        
        如果说八月十五是人间的团圆，七月十五就是冥间的团圆了吧。
        
        元蘅一杯，栾景一杯，阿姻一杯，二伯一杯，阿嬷一杯，子鸢一杯，还有许妃的那个宫人，虽然是风家的钉子，但毕竟是舍命的忠诚，也敬她一杯。
        
        作者：少鬼无忧
        链接：https://www.jianshu.com/p/565fbcc8aebb
        来源：简书
        著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。`,
        good: 0,
        bad: 0,
        comment: 0,
    },
    {
        title: `秦乐园【二】`,
        time: `2020.06.21 11:22:13`,
        author: `南山顽石`,
        content: `一个月后，我终于又回到了临淄，除了以长辈的名义召开了宗室会议，把宗主的位置传给了曦儿，没有公开露过面。反正只要有高权限的令牌，在哪里都通行无阻。

        再好的人皮面具，一天也只能戴几个时辰。所以我白天基本就是在房里学琴、对弈。闷久了就去看排舞。我甚至还作了一首歌，只是曲意悲怆，音调极高，不是能传于市井的曲子。
        
        回来这么久，一直没有流枫的消息。我的意思是想他回哥哥那里，或者去爹爹那里也不错。当然，他本来就是四叔从杀手里简拔出来的，要是想继续做杀手，也不失为正途。
        
        转眼就到了七月半。我现在也算是半人半鬼，到了清明和鬼节，总要享受一下反客为主的乐趣。
        
        我戴上人皮面具，拎了酒，找了个观景位置不错的高台，一边等着月亮出来一边自斟自饮。
        
        如果说八月十五是人间的团圆，七月十五就是冥间的团圆了吧。
        
        元蘅一杯，栾景一杯，阿姻一杯，二伯一杯，阿嬷一杯，子鸢一杯，还有许妃的那个宫人，虽然是风家的钉子，但毕竟是舍命的忠诚，也敬她一杯。
        
        作者：少鬼无忧
        链接：https://www.jianshu.com/p/565fbcc8aebb
        来源：简书
        著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。`,
        good: 0,
        bad: 0,
        comment: 0,
    },
    {
        title: `秦乐园【三】`,
        time: `2020.06.21 11:22:13`,
        author: `南山顽石`,
        content: `一个月后，我终于又回到了临淄，除了以长辈的名义召开了宗室会议，把宗主的位置传给了曦儿，没有公开露过面。反正只要有高权限的令牌，在哪里都通行无阻。

        再好的人皮面具，一天也只能戴几个时辰。所以我白天基本就是在房里学琴、对弈。闷久了就去看排舞。我甚至还作了一首歌，只是曲意悲怆，音调极高，不是能传于市井的曲子。
        
        回来这么久，一直没有流枫的消息。我的意思是想他回哥哥那里，或者去爹爹那里也不错。当然，他本来就是四叔从杀手里简拔出来的，要是想继续做杀手，也不失为正途。
        
        转眼就到了七月半。我现在也算是半人半鬼，到了清明和鬼节，总要享受一下反客为主的乐趣。
        
        我戴上人皮面具，拎了酒，找了个观景位置不错的高台，一边等着月亮出来一边自斟自饮。
        
        如果说八月十五是人间的团圆，七月十五就是冥间的团圆了吧。
        
        元蘅一杯，栾景一杯，阿姻一杯，二伯一杯，阿嬷一杯，子鸢一杯，还有许妃的那个宫人，虽然是风家的钉子，但毕竟是舍命的忠诚，也敬她一杯。
        
        作者：少鬼无忧
        链接：https://www.jianshu.com/p/565fbcc8aebb
        来源：简书
        著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。`,
        good: 0,
        bad: 0,
        comment: 0,
    },
    {
        title: `秦乐园【四】`,
        time: `2020.06.21 11:22:13`,
        author: `南山顽石`,
        content: `一个月后，我终于又回到了临淄，除了以长辈的名义召开了宗室会议，把宗主的位置传给了曦儿，没有公开露过面。反正只要有高权限的令牌，在哪里都通行无阻。

        再好的人皮面具，一天也只能戴几个时辰。所以我白天基本就是在房里学琴、对弈。闷久了就去看排舞。我甚至还作了一首歌，只是曲意悲怆，音调极高，不是能传于市井的曲子。
        
        回来这么久，一直没有流枫的消息。我的意思是想他回哥哥那里，或者去爹爹那里也不错。当然，他本来就是四叔从杀手里简拔出来的，要是想继续做杀手，也不失为正途。
        
        转眼就到了七月半。我现在也算是半人半鬼，到了清明和鬼节，总要享受一下反客为主的乐趣。
        
        我戴上人皮面具，拎了酒，找了个观景位置不错的高台，一边等着月亮出来一边自斟自饮。
        
        如果说八月十五是人间的团圆，七月十五就是冥间的团圆了吧。
        
        元蘅一杯，栾景一杯，阿姻一杯，二伯一杯，阿嬷一杯，子鸢一杯，还有许妃的那个宫人，虽然是风家的钉子，但毕竟是舍命的忠诚，也敬她一杯。
        
        作者：少鬼无忧
        链接：https://www.jianshu.com/p/565fbcc8aebb
        来源：简书
        著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。`,
        good: 0,
        bad: 0,
        comment: 0,
    },
    {
        title: `秦乐园【五】`,
        time: `2020.06.21 11:22:13`,
        author: `南山顽石`,
        content: `一个月后，我终于又回到了临淄，除了以长辈的名义召开了宗室会议，把宗主的位置传给了曦儿，没有公开露过面。反正只要有高权限的令牌，在哪里都通行无阻。

        再好的人皮面具，一天也只能戴几个时辰。所以我白天基本就是在房里学琴、对弈。闷久了就去看排舞。我甚至还作了一首歌，只是曲意悲怆，音调极高，不是能传于市井的曲子。
        
        回来这么久，一直没有流枫的消息。我的意思是想他回哥哥那里，或者去爹爹那里也不错。当然，他本来就是四叔从杀手里简拔出来的，要是想继续做杀手，也不失为正途。
        
        转眼就到了七月半。我现在也算是半人半鬼，到了清明和鬼节，总要享受一下反客为主的乐趣。
        
        我戴上人皮面具，拎了酒，找了个观景位置不错的高台，一边等着月亮出来一边自斟自饮。
        
        如果说八月十五是人间的团圆，七月十五就是冥间的团圆了吧。
        
        元蘅一杯，栾景一杯，阿姻一杯，二伯一杯，阿嬷一杯，子鸢一杯，还有许妃的那个宫人，虽然是风家的钉子，但毕竟是舍命的忠诚，也敬她一杯。
        
        作者：少鬼无忧
        链接：https://www.jianshu.com/p/565fbcc8aebb
        来源：简书
        著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。`,
        good: 0,
        bad: 0,
        comment: 0,
    },
]
var httpObjArr = [
    {
        title: `http【一】`,
        time: `2020.06.21 11:22:13`,
        author: `人生海海`,
        content: `这是罗曼·罗兰在《米开朗基罗传》中所写的一句话。

        而这句话不知鼓舞了多少在生活的沼泽中艰难前行的人们，也不知成就了多少真正的英雄。
        
        那么，到底什么是生活的真相？我们又要在生活中成为怎样的人？
        
        知乎网友@人间草木给出了这样的回答：
        
        “所谓生活的真相，就是告诉你一切都是没有价值的，只是在于你的选择。
        
        而此时继续选择英雄主义就变得无比困难，因为这比犬儒困难得多，于是这就成为了真正的英雄主义。
        
        何种生活态度？
        
        认清真相之前的英雄主义是在庞大的价值基础上延续价值。`,
        good: 0,
        bad: 0,
        comment: 0,
    },
    {
        title: `http【二】`,
        time: `2020.06.21 11:22:13`,
        author: `人生海海`,
        content: `这是罗曼·罗兰在《米开朗基罗传》中所写的一句话。

        而这句话不知鼓舞了多少在生活的沼泽中艰难前行的人们，也不知成就了多少真正的英雄。
        
        那么，到底什么是生活的真相？我们又要在生活中成为怎样的人？
        
        知乎网友@人间草木给出了这样的回答：
        
        “所谓生活的真相，就是告诉你一切都是没有价值的，只是在于你的选择。
        
        而此时继续选择英雄主义就变得无比困难，因为这比犬儒困难得多，于是这就成为了真正的英雄主义。
        
        何种生活态度？
        
        认清真相之前的英雄主义是在庞大的价值基础上延续价值`,
        good: 0,
        bad: 0,
        comment: 0,
    },
    {
        title: `http【三】`,
        time: `2020.06.21 11:22:13`,
        author: `人生海海`,
        content: `这是罗曼·罗兰在《米开朗基罗传》中所写的一句话。

        而这句话不知鼓舞了多少在生活的沼泽中艰难前行的人们，也不知成就了多少真正的英雄。
        
        那么，到底什么是生活的真相？我们又要在生活中成为怎样的人？
        
        知乎网友@人间草木给出了这样的回答：
        
        “所谓生活的真相，就是告诉你一切都是没有价值的，只是在于你的选择。
        
        而此时继续选择英雄主义就变得无比困难，因为这比犬儒困难得多，于是这就成为了真正的英雄主义。
        
        何种生活态度？
        
        认清真相之前的英雄主义是在庞大的价值基础上延续价值`,
        good: 0,
        bad: 0,
        comment: 0,
    },
    {
        title: `http【四】`,
        time: `2020.06.21 11:22:13`,
        author: `人生海海`,
        content: `这是罗曼·罗兰在《米开朗基罗传》中所写的一句话。

        而这句话不知鼓舞了多少在生活的沼泽中艰难前行的人们，也不知成就了多少真正的英雄。
        
        那么，到底什么是生活的真相？我们又要在生活中成为怎样的人？
        
        知乎网友@人间草木给出了这样的回答：
        
        “所谓生活的真相，就是告诉你一切都是没有价值的，只是在于你的选择。
        
        而此时继续选择英雄主义就变得无比困难，因为这比犬儒困难得多，于是这就成为了真正的英雄主义。
        
        何种生活态度？
        
        认清真相之前的英雄主义是在庞大的价值基础上延续价值`,
        good: 0,
        bad: 0,
        comment: 0,
    },
    {
        title: `http【五】`,
        time: `2020.06.21 11:22:13`,
        author: `人生海海`,
        content: `这是罗曼·罗兰在《米开朗基罗传》中所写的一句话。

        而这句话不知鼓舞了多少在生活的沼泽中艰难前行的人们，也不知成就了多少真正的英雄。
        
        那么，到底什么是生活的真相？我们又要在生活中成为怎样的人？
        
        知乎网友@人间草木给出了这样的回答：
        
        “所谓生活的真相，就是告诉你一切都是没有价值的，只是在于你的选择。
        
        而此时继续选择英雄主义就变得无比困难，因为这比犬儒困难得多，于是这就成为了真正的英雄主义。
        
        何种生活态度？
        
        认清真相之前的英雄主义是在庞大的价值基础上延续价值`,
        good: 0,
        bad: 0,
        comment: 0,
    }
]
var ajaxObjArr = [
    {
        title: `ajax【一】`,
        time: `2020.06.21 11:22:13`,
        author: `杀死一只知更鸟`,
        content: `如果你问我有什么小说是特别让人难忘的吗？我一定会毫不犹豫的告诉你《杀死一只知更鸟》是我读过的最佳的小说之一，很可能去掉之一。在我看来《杀死一只知更鸟》是一本不折不扣的父亲修炼手册，夸张一点说，世上的所有男性，不管当没当父亲，一定要看《杀死一只知更鸟》。

        看完或许你会跟我发出一样的感叹，今生要成为阿迪克斯那样的父亲。作为一名白人律师，阿迪克斯为黑人辩护，所有人包括他女儿也不理解这种行为。而他身体力行，保持着宽容怜悯的立场。每天晚上陪着女儿斯库特阅读，告诉儿子和女儿什么是勇敢，如何在丑陋的世界保持自己的道德底线。这本小说对男人的成长极具塑造性，对任何承担一个父亲的角色，起到了卓越的示范作用。那段有关勇敢的话，在书中异常闪亮：“勇敢就是，在你还没开始的时候就知道自己注定会输，但依然义无反顾地去做，并且不管发生什么都坚持到底。一个人很少能赢，但也总会有赢的时候。”`,
        good: 0,
        bad: 0,
        comment: 0,
    },
    {
        title: `ajax【二】`,
        time: `2020.06.21 11:22:13`,
        author: `杀死一只知更鸟`,
        content: `如果你问我有什么小说是特别让人难忘的吗？我一定会毫不犹豫的告诉你《杀死一只知更鸟》是我读过的最佳的小说之一，很可能去掉之一。在我看来《杀死一只知更鸟》是一本不折不扣的父亲修炼手册，夸张一点说，世上的所有男性，不管当没当父亲，一定要看《杀死一只知更鸟》。

        看完或许你会跟我发出一样的感叹，今生要成为阿迪克斯那样的父亲。作为一名白人律师，阿迪克斯为黑人辩护，所有人包括他女儿也不理解这种行为。而他身体力行，保持着宽容怜悯的立场。每天晚上陪着女儿斯库特阅读，告诉儿子和女儿什么是勇敢，如何在丑陋的世界保持自己的道德底线。这本小说对男人的成长极具塑造性，对任何承担一个父亲的角色，起到了卓越的示范作用。那段有关勇敢的话，在书中异常闪亮：“勇敢就是，在你还没开始的时候就知道自己注定会输，但依然义无反顾地去做，并且不管发生什么都坚持到底。一个人很少能赢，但也总会有赢的时候。”`,
        good: 0,
        bad: 0,
        comment: 0,
    },
    {
        title: `ajax【三】`,
        time: `2020.06.21 11:22:13`,
        author: `杀死一只知更鸟`,
        content: `如果你问我有什么小说是特别让人难忘的吗？我一定会毫不犹豫的告诉你《杀死一只知更鸟》是我读过的最佳的小说之一，很可能去掉之一。在我看来《杀死一只知更鸟》是一本不折不扣的父亲修炼手册，夸张一点说，世上的所有男性，不管当没当父亲，一定要看《杀死一只知更鸟》。

        看完或许你会跟我发出一样的感叹，今生要成为阿迪克斯那样的父亲。作为一名白人律师，阿迪克斯为黑人辩护，所有人包括他女儿也不理解这种行为。而他身体力行，保持着宽容怜悯的立场。每天晚上陪着女儿斯库特阅读，告诉儿子和女儿什么是勇敢，如何在丑陋的世界保持自己的道德底线。这本小说对男人的成长极具塑造性，对任何承担一个父亲的角色，起到了卓越的示范作用。那段有关勇敢的话，在书中异常闪亮：“勇敢就是，在你还没开始的时候就知道自己注定会输，但依然义无反顾地去做，并且不管发生什么都坚持到底。一个人很少能赢，但也总会有赢的时候。”。`,
        good: 0,
        bad: 0,
        comment: 0,
    },
    {
        title: `ajax【四】`,
        time: `2020.06.21 11:22:13`,
        author: `杀死一只知更鸟`,
        content: `如果你问我有什么小说是特别让人难忘的吗？我一定会毫不犹豫的告诉你《杀死一只知更鸟》是我读过的最佳的小说之一，很可能去掉之一。在我看来《杀死一只知更鸟》是一本不折不扣的父亲修炼手册，夸张一点说，世上的所有男性，不管当没当父亲，一定要看《杀死一只知更鸟》。

        看完或许你会跟我发出一样的感叹，今生要成为阿迪克斯那样的父亲。作为一名白人律师，阿迪克斯为黑人辩护，所有人包括他女儿也不理解这种行为。而他身体力行，保持着宽容怜悯的立场。每天晚上陪着女儿斯库特阅读，告诉儿子和女儿什么是勇敢，如何在丑陋的世界保持自己的道德底线。这本小说对男人的成长极具塑造性，对任何承担一个父亲的角色，起到了卓越的示范作用。那段有关勇敢的话，在书中异常闪亮：“勇敢就是，在你还没开始的时候就知道自己注定会输，但依然义无反顾地去做，并且不管发生什么都坚持到底。一个人很少能赢，但也总会有赢的时候。”`,
        good: 0,
        bad: 0,
        comment: 0,
    },
    {
        title: `ajax【五】`,
        time: `2020.06.21 11:22:13`,
        author: `杀死一只知更鸟`,
        content: `如果你问我有什么小说是特别让人难忘的吗？我一定会毫不犹豫的告诉你《杀死一只知更鸟》是我读过的最佳的小说之一，很可能去掉之一。在我看来《杀死一只知更鸟》是一本不折不扣的父亲修炼手册，夸张一点说，世上的所有男性，不管当没当父亲，一定要看《杀死一只知更鸟》。

        看完或许你会跟我发出一样的感叹，今生要成为阿迪克斯那样的父亲。作为一名白人律师，阿迪克斯为黑人辩护，所有人包括他女儿也不理解这种行为。而他身体力行，保持着宽容怜悯的立场。每天晚上陪着女儿斯库特阅读，告诉儿子和女儿什么是勇敢，如何在丑陋的世界保持自己的道德底线。这本小说对男人的成长极具塑造性，对任何承担一个父亲的角色，起到了卓越的示范作用。那段有关勇敢的话，在书中异常闪亮：“勇敢就是，在你还没开始的时候就知道自己注定会输，但依然义无反顾地去做，并且不管发生什么都坚持到底。一个人很少能赢，但也总会有赢的时候。”`,
        good: 0,
        bad: 0,
        comment: 0,
    }
]
var expressObjArr = [
    {
        title: `express【一】`,
        time: `2020.06.21 11:22:13`,
        author: `海子`,
        content: `1.我有一所房子，面朝大海，春暖花开。——海子

        2.青海湖不远，湖畔一捆捆蜂箱，使我显得凄凄迷人，青草开满鲜花。——海子《七月不远》
        
        3.风后面是风，
        
        天空上面还是天空，
        
        道路前面还是道路。——海子《四姐妹》
        
        4.黑夜从大地上升起
        
        遮住了光明的天空
        
        丰收后荒凉的大地
        
        黑夜从你内部升起
        
        你从远方来， 我到远方去
        
        遥远的路程经过这里
        
        天空一无所有
        
        为何给我安慰
        
        丰收之后荒凉的大地
        
        人们取走了一年的收成
        
        取走了粮食骑走了马
        
        留在地里的人， 埋的很深
        
        草叉闪闪发亮， 稻草堆在火上
        
        稻谷堆在黑暗的谷仓
        
        谷仓中太黑暗， 太寂静， 太丰收
        
        也太荒凉。——海子《黑夜的献诗》`,
        good: 0,
        bad: 0,
        comment: 0,
    },
    {
        title: `express【二】`,
        time: `2020.06.21 11:22:13`,
        author: `海子`,
        content: `1.我有一所房子，面朝大海，春暖花开。——海子

        2.青海湖不远，湖畔一捆捆蜂箱，使我显得凄凄迷人，青草开满鲜花。——海子《七月不远》
        
        3.风后面是风，
        
        天空上面还是天空，
        
        道路前面还是道路。——海子《四姐妹》
        
        4.黑夜从大地上升起
        
        遮住了光明的天空
        
        丰收后荒凉的大地
        
        黑夜从你内部升起
        
        你从远方来， 我到远方去
        
        遥远的路程经过这里
        
        天空一无所有
        
        为何给我安慰
        
        丰收之后荒凉的大地
        
        人们取走了一年的收成
        
        取走了粮食骑走了马
        
        留在地里的人， 埋的很深
        
        草叉闪闪发亮， 稻草堆在火上
        
        稻谷堆在黑暗的谷仓
        
        谷仓中太黑暗， 太寂静， 太丰收
        
        也太荒凉。——海子《黑夜的献诗》`,
        good: 0,
        bad: 0,
        comment: 0,
    },
    {
        title: `express【三】`,
        time: `2020.06.21 11:22:13`,
        author: `海子`,
        content: `1.我有一所房子，面朝大海，春暖花开。——海子

        2.青海湖不远，湖畔一捆捆蜂箱，使我显得凄凄迷人，青草开满鲜花。——海子《七月不远》
        
        3.风后面是风，
        
        天空上面还是天空，
        
        道路前面还是道路。——海子《四姐妹》
        
        4.黑夜从大地上升起
        
        遮住了光明的天空
        
        丰收后荒凉的大地
        
        黑夜从你内部升起
        
        你从远方来， 我到远方去
        
        遥远的路程经过这里
        
        天空一无所有
        
        为何给我安慰
        
        丰收之后荒凉的大地
        
        人们取走了一年的收成
        
        取走了粮食骑走了马
        
        留在地里的人， 埋的很深
        
        草叉闪闪发亮， 稻草堆在火上
        
        稻谷堆在黑暗的谷仓
        
        谷仓中太黑暗， 太寂静， 太丰收
        
        也太荒凉。——海子《黑夜的献诗》`,
        good: 0,
        bad: 0,
        comment: 0,
    },
    {
        title: `express【四】`,
        time: `2020.06.21 11:22:13`,
        author: `海子`,
        content: `1.我有一所房子，面朝大海，春暖花开。——海子

        2.青海湖不远，湖畔一捆捆蜂箱，使我显得凄凄迷人，青草开满鲜花。——海子《七月不远》
        
        3.风后面是风，
        
        天空上面还是天空，
        
        道路前面还是道路。——海子《四姐妹》
        
        4.黑夜从大地上升起
        
        遮住了光明的天空
        
        丰收后荒凉的大地
        
        黑夜从你内部升起
        
        你从远方来， 我到远方去
        
        遥远的路程经过这里
        
        天空一无所有
        
        为何给我安慰
        
        丰收之后荒凉的大地
        
        人们取走了一年的收成
        
        取走了粮食骑走了马
        
        留在地里的人， 埋的很深
        
        草叉闪闪发亮， 稻草堆在火上
        
        稻谷堆在黑暗的谷仓
        
        谷仓中太黑暗， 太寂静， 太丰收
        
        也太荒凉。——海子《黑夜的献诗》`,
        good: 0,
        bad: 0,
        comment: 0,
    },
    {
        title: `express【五】`,
        time: `2020.06.21 11:22:13`,
        author: `海子`,
        content: `1.我有一所房子，面朝大海，春暖花开。——海子

        2.青海湖不远，湖畔一捆捆蜂箱，使我显得凄凄迷人，青草开满鲜花。——海子《七月不远》
        
        3.风后面是风，
        
        天空上面还是天空，
        
        道路前面还是道路。——海子《四姐妹》
        
        4.黑夜从大地上升起
        
        遮住了光明的天空
        
        丰收后荒凉的大地
        
        黑夜从你内部升起
        
        你从远方来， 我到远方去
        
        遥远的路程经过这里
        
        天空一无所有
        
        为何给我安慰
        
        丰收之后荒凉的大地
        
        人们取走了一年的收成
        
        取走了粮食骑走了马
        
        留在地里的人， 埋的很深
        
        草叉闪闪发亮， 稻草堆在火上
        
        稻谷堆在黑暗的谷仓
        
        谷仓中太黑暗， 太寂静， 太丰收
        
        也太荒凉。——海子《黑夜的献诗》`,
        good: 0,
        bad: 0,
        comment: 0,
    }
]
var mongoObjArr = [
    {
        title: `mongo【一】`,
        time: `2020.06.21 11:22:13`,
        author: `小时代`,
        content: `作者：xelloss
        链接：https://www.zhihu.com/question/356722701/answer/904020945
        来源：知乎
        著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
        
        上海本来就是中国大陆地区西化最强的地方。这个没人会否认。不过小时代几乎把上海拍的真
        像欧美了。取景设定上，姐妹花住的是思南路的独栋洋房。学校则是在松江泰晤士小镇。
        宫洺的宅子则是玻璃房。他的生日会在外滩源的英国领事馆旧址。第一部最后那场服装发
        布会则是在老场坊，一个从设计到使用水泥都来自英国的建筑。第二部顾源订婚会在中苏
        友好大厦。奢侈品那就更不用说了。Prada，Amani到处飞。压根没提到任何中国本土的品牌。
        剧中人穿的，玩的场所更是西化的无以复加。如果说前面是为了展现上海人如何热衷于使用国外奢
        侈品品牌，而才用了一些夸张手法的话。那后面这个就是纯粹扯淡了。
        剧中，这些人连死都要死的像个西方人。周崇光的葬礼在佘山教堂，顾里的父亲死后埋在基督教墓地。光看那个墓地的环境，我还以为在美国阿灵顿公墓呢。我对天发誓，上海的上流社会，绝对不存在那么多虔诚的基督教徒。我作为上海人，对于故乡的洋气当然是有自豪感的。我很小时，爸妈就带我吃过西餐。稍大点吃日料。我对教堂的好感也远胜于寺庙。但我同样也是吃着生煎，油条，泡饭，牛肉拉面长大的。我的血管里终究还是流着中国人的血。导演为啥要把取景做的那么极端西化，以至于让我一个上海人在电影里几乎感受不到上海这座城市中国的一面。答案恐怕只有两个字：装 b`,
        good: 0,
        bad: 0,
        comment: 0,
    },
    {
        title: `mongo【二】`,
        time: `2020.06.21 11:22:13`,
        author: `小时代`,
        content: `作者：xelloss
        链接：https://www.zhihu.com/question/356722701/answer/904020945
        来源：知乎
        著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
        
        上海本来就是中国大陆地区西化最强的地方。这个没人会否认。不过小时代几乎把上海拍的真像欧美了。取景设定上，姐妹花住的是思南路的独栋洋房。学校则是在松江泰晤士小镇。宫洺的宅子则是玻璃房。他的生日会在外滩源的英国领事馆旧址。第一部最后那场服装发布会则是在老场坊，一个从设计到使用水泥都来自英国的建筑。第二部顾源订婚会在中苏友好大厦。奢侈品那就更不用说了。Prada，Amani到处飞。压根没提到任何中国本土的品牌。剧中人穿的，玩的场所更是西化的无以复加。如果说前面是为了展现上海人如何热衷于使用国外奢侈品品牌，而才用了一些夸张手法的话。那后面这个就是纯粹扯淡了。剧中，这些人连死都要死的像个西方人。周崇光的葬礼在佘山教堂，顾里的父亲死后埋在基督教墓地。光看那个墓地的环境，我还以为在美国阿灵顿公墓呢。我对天发誓，上海的上流社会，绝对不存在那么多虔诚的基督教徒。我作为上海人，对于故乡的洋气当然是有自豪感的。我很小时，爸妈就带我吃过西餐。稍大点吃日料。我对教堂的好感也远胜于寺庙。但我同样也是吃着生煎，油条，泡饭，牛肉拉面长大的。我的血管里终究还是流着中国人的血。导演为啥要把取景做的那么极端西化，以至于让我一个上海人在电影里几乎感受不到上海这座城市中国的一面。答案恐怕只有两个字：装 b`,
        good: 0,
        bad: 0,
        comment: 0,
    },
    {
        title: `mongo【三】`,
        time: `2020.06.21 11:22:13`,
        author: `小时代`,
        content: `作者：xelloss
        链接：https://www.zhihu.com/question/356722701/answer/904020945
        来源：知乎
        著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
        
        上海本来就是中国大陆地区西化最强的地方。这个没人会否认。不过小时代几乎把上海拍的真像欧美了。取景设定上，姐妹花住的是思南路的独栋洋房。学校则是在松江泰晤士小镇。宫洺的宅子则是玻璃房。他的生日会在外滩源的英国领事馆旧址。第一部最后那场服装发布会则是在老场坊，一个从设计到使用水泥都来自英国的建筑。第二部顾源订婚会在中苏友好大厦。奢侈品那就更不用说了。Prada，Amani到处飞。压根没提到任何中国本土的品牌。剧中人穿的，玩的场所更是西化的无以复加。如果说前面是为了展现上海人如何热衷于使用国外奢侈品品牌，而才用了一些夸张手法的话。那后面这个就是纯粹扯淡了。剧中，这些人连死都要死的像个西方人。周崇光的葬礼在佘山教堂，顾里的父亲死后埋在基督教墓地。光看那个墓地的环境，我还以为在美国阿灵顿公墓呢。我对天发誓，上海的上流社会，绝对不存在那么多虔诚的基督教徒。我作为上海人，对于故乡的洋气当然是有自豪感的。我很小时，爸妈就带我吃过西餐。稍大点吃日料。我对教堂的好感也远胜于寺庙。但我同样也是吃着生煎，油条，泡饭，牛肉拉面长大的。我的血管里终究还是流着中国人的血。导演为啥要把取景做的那么极端西化，以至于让我一个上海人在电影里几乎感受不到上海这座城市中国的一面。答案恐怕只有两个字：装 b`,
        good: 0,
        bad: 0,
        comment: 0,
    },
    {
        title: `mongo【四】`,
        time: `2020.06.21 11:22:13`,
        author: `小时代`,
        content: `作者：xelloss
        链接：https://www.zhihu.com/question/356722701/answer/904020945
        来源：知乎
        著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
        
        上海本来就是中国大陆地区西化最强的地方。这个没人会否认。不过小时代几乎把上海拍的真像欧美了。取景设定上，姐妹花住的是思南路的独栋洋房。学校则是在松江泰晤士小镇。宫洺的宅子则是玻璃房。他的生日会在外滩源的英国领事馆旧址。第一部最后那场服装发布会则是在老场坊，一个从设计到使用水泥都来自英国的建筑。第二部顾源订婚会在中苏友好大厦。奢侈品那就更不用说了。Prada，Amani到处飞。压根没提到任何中国本土的品牌。剧中人穿的，玩的场所更是西化的无以复加。如果说前面是为了展现上海人如何热衷于使用国外奢侈品品牌，而才用了一些夸张手法的话。那后面这个就是纯粹扯淡了。剧中，这些人连死都要死的像个西方人。周崇光的葬礼在佘山教堂，顾里的父亲死后埋在基督教墓地。光看那个墓地的环境，我还以为在美国阿灵顿公墓呢。我对天发誓，上海的上流社会，绝对不存在那么多虔诚的基督教徒。我作为上海人，对于故乡的洋气当然是有自豪感的。我很小时，爸妈就带我吃过西餐。稍大点吃日料。我对教堂的好感也远胜于寺庙。但我同样也是吃着生煎，油条，泡饭，牛肉拉面长大的。我的血管里终究还是流着中国人的血。导演为啥要把取景做的那么极端西化，以至于让我一个上海人在电影里几乎感受不到上海这座城市中国的一面。答案恐怕只有两个字：装 b`,
        good: 0,
        bad: 0,
        comment: 0,
    },
    {
        title: `mongo【五】`,
        time: `2020.06.21 11:22:13`,
        author: `小时代`,
        content: `作者：xelloss
        链接：https://www.zhihu.com/question/356722701/answer/904020945
        来源：知乎
        著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
        
        上海本来就是中国大陆地区西化最强的地方。这个没人会否认。不过小时代几乎把上海拍的真像欧美了。取景设定上，姐妹花住的是思南路的独栋洋房。学校则是在松江泰晤士小镇。宫洺的宅子则是玻璃房。他的生日会在外滩源的英国领事馆旧址。第一部最后那场服装发布会则是在老场坊，一个从设计到使用水泥都来自英国的建筑。第二部顾源订婚会在中苏友好大厦。奢侈品那就更不用说了。Prada，Amani到处飞。压根没提到任何中国本土的品牌。剧中人穿的，玩的场所更是西化的无以复加。如果说前面是为了展现上海人如何热衷于使用国外奢侈品品牌，而才用了一些夸张手法的话。那后面这个就是纯粹扯淡了。剧中，这些人连死都要死的像个西方人。周崇光的葬礼在佘山教堂，顾里的父亲死后埋在基督教墓地。光看那个墓地的环境，我还以为在美国阿灵顿公墓呢。我对天发誓，上海的上流社会，绝对不存在那么多虔诚的基督教徒。我作为上海人，对于故乡的洋气当然是有自豪感的。我很小时，爸妈就带我吃过西餐。稍大点吃日料。我对教堂的好感也远胜于寺庙。但我同样也是吃着生煎，油条，泡饭，牛肉拉面长大的。我的血管里终究还是流着中国人的血。导演为啥要把取景做的那么极端西化，以至于让我一个上海人在电影里几乎感受不到上海这座城市中国的一面。答案恐怕只有两个字：装 b`,
        good: 0,
        bad: 0,
        comment: 0,
    }
]
var jqObjArr = [
    {
        title: `jq【一】`,
        time: `2020.06.21 11:22:13`,
        author: `霸王别姬`,
        content: `我怀疑他娘亲都不是亲的，因为窑子出生的孩子，懂事了之后谁养他他就叫谁娘，但未必是亲生的母亲。蒋雯丽把他带到关师傅那里，就没打算带他回去，所以才会那么决绝的把手指头切了，亲生母亲未必做得出这么壮烈的事情，这里不要他，换个地方保管下也可以。但小豆子没有退路，他必须留在戏院，因为这里是要签契约，并且生死不顾，可以彻底切断跟过去关系的地方。刚露面的时候，他满脸都是伤，明显是被打过。原因有很多，可能是被老鸨子发现是男孩子，毒打一顿然后赶了出来。也可能是娘要带他离开，他不愿意离开，所以被娘打了一顿。所以戏园子也好，窑子也好，都是程蝶衣寻找自我的过程，但这里面都充满了背叛，导致程蝶衣最后也没有健全的认知自我。

        作者：张含
        链接：https://www.zhihu.com/question/34575915/answer/867674396
        来源：知乎
        著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。`,
        good: 0,
        bad: 0,
        comment: 0,
    },
    {
        title: `jq【二】`,
        time: `2020.06.21 11:22:13`,
        author: `霸王别姬`,
        content: `我怀疑他娘亲都不是亲的，因为窑子出生的孩子，懂事了之后谁养他他就叫谁娘，但未必是亲生的母亲。蒋雯丽把他带到关师傅那里，就没打算带他回去，所以才会那么决绝的把手指头切了，亲生母亲未必做得出这么壮烈的事情，这里不要他，换个地方保管下也可以。但小豆子没有退路，他必须留在戏院，因为这里是要签契约，并且生死不顾，可以彻底切断跟过去关系的地方。刚露面的时候，他满脸都是伤，明显是被打过。原因有很多，可能是被老鸨子发现是男孩子，毒打一顿然后赶了出来。也可能是娘要带他离开，他不愿意离开，所以被娘打了一顿。所以戏园子也好，窑子也好，都是程蝶衣寻找自我的过程，但这里面都充满了背叛，导致程蝶衣最后也没有健全的认知自我。

        作者：张含
        链接：https://www.zhihu.com/question/34575915/answer/867674396
        来源：知乎
        著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。`,
        good: 0,
        bad: 0,
        comment: 0,
    },
    {
        title: `jq【三】`,
        time: `2020.06.21 11:22:13`,
        author: `霸王别姬`,
        content: `我怀疑他娘亲都不是亲的，因为窑子出生的孩子，懂事了之后谁养他他就叫谁娘，但未必是亲生的母亲。蒋雯丽把他带到关师傅那里，就没打算带他回去，所以才会那么决绝的把手指头切了，亲生母亲未必做得出这么壮烈的事情，这里不要他，换个地方保管下也可以。但小豆子没有退路，他必须留在戏院，因为这里是要签契约，并且生死不顾，可以彻底切断跟过去关系的地方。刚露面的时候，他满脸都是伤，明显是被打过。原因有很多，可能是被老鸨子发现是男孩子，毒打一顿然后赶了出来。也可能是娘要带他离开，他不愿意离开，所以被娘打了一顿。所以戏园子也好，窑子也好，都是程蝶衣寻找自我的过程，但这里面都充满了背叛，导致程蝶衣最后也没有健全的认知自我。

        作者：张含
        链接：https://www.zhihu.com/question/34575915/answer/867674396
        来源：知乎
        著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。`,
        good: 0,
        bad: 0,
        comment: 0,
    },
    {
        title: `jq【四】`,
        time: `2020.06.21 11:22:13`,
        author: `霸王别姬`,
        content: `我怀疑他娘亲都不是亲的，因为窑子出生的孩子，懂事了之后谁养他他就叫谁娘，但未必是亲生的母亲。蒋雯丽把他带到关师傅那里，就没打算带他回去，所以才会那么决绝的把手指头切了，亲生母亲未必做得出这么壮烈的事情，这里不要他，换个地方保管下也可以。但小豆子没有退路，他必须留在戏院，因为这里是要签契约，并且生死不顾，可以彻底切断跟过去关系的地方。刚露面的时候，他满脸都是伤，明显是被打过。原因有很多，可能是被老鸨子发现是男孩子，毒打一顿然后赶了出来。也可能是娘要带他离开，他不愿意离开，所以被娘打了一顿。所以戏园子也好，窑子也好，都是程蝶衣寻找自我的过程，但这里面都充满了背叛，导致程蝶衣最后也没有健全的认知自我。

        作者：张含
        链接：https://www.zhihu.com/question/34575915/answer/867674396
        来源：知乎
        著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。`,
        good: 0,
        bad: 0,
        comment: 0,
    },
    {
        title: `jq【五】`,
        time: `2020.06.21 11:22:13`,
        author: `南山顽石`,
        content: `我怀疑他娘亲都不是亲的，因为窑子出生的孩子，懂事了之后谁养他他就叫谁娘，但未必是亲生的母亲。蒋雯丽把他带到关师傅那里，就没打算带他回去，所以才会那么决绝的把手指头切了，亲生母亲未必做得出这么壮烈的事情，这里不要他，换个地方保管下也可以。但小豆子没有退路，他必须留在戏院，因为这里是要签契约，并且生死不顾，可以彻底切断跟过去关系的地方。刚露面的时候，他满脸都是伤，明显是被打过。原因有很多，可能是被老鸨子发现是男孩子，毒打一顿然后赶了出来。也可能是娘要带他离开，他不愿意离开，所以被娘打了一顿。所以戏园子也好，窑子也好，都是程蝶衣寻找自我的过程，但这里面都充满了背叛，导致程蝶衣最后也没有健全的认知自我。

        作者：张含
        链接：https://www.zhihu.com/question/34575915/answer/867674396
        来源：知乎
        著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。`,
        good: 0,
        bad: 0,
        comment: 0,
    }
]
var reactObjArr = [
    {
        title: `react【一】`,
        time: `2020.06.21 11:22:13`,
        author: `南山顽石`,
        content: `恰好亮着灯，且从窗户传出阵阵嘻嘻哈哈的说话声，甚是热闹。敲开门时，却见除了红缨外，汪衍华、孙老师也在，每个人手里都拿着一把牌，原来他们在玩弥竹竿。衍举看了衍华一眼，嘿嘿一笑说：“衍华，今儿黑跟你媳妇同房呀？”张红缨胀得满面通红，但知道汪衍举跟谁说话都是这个样子，没轻没重的，她便不好怪病他，只得讪笑着说：“哪呢！等给孙校长把房子收拾好，天已经不早了。所以衍华就不回去了，今儿黑他在孙校长那儿歇。衍举哥也真是的！我跟衍华都是规规矩矩的老实疙瘩子，你把我俩当啥人了呢？”衍举道：“我说得耍呢。”

        　　衍平道：“红缨，我们也是无事不登三宝殿，是有个事想叫你帮忙。”接着便前拉后扯的说了何秀莲叫水吹走了，他们沿河寻尸首，以及汪耀林叫蛇咬了，被送到县医院去了等语，最后方说想请她去耀臣那儿帮忙做饭。听到何秀莲的死讯，红缨、孙老师和王衍华均极为吃惊。红缨道：“早上到河边送孙校长时，还活蹦乱跳的一个人，咋就叫水吹去了呢？秀莲实在是命苦……”说着说着眼睛就湿了，急忙掏出手绢擦了擦眼角。孙老师皱眉沉思片刻说：“俗话说活要见人，死要见尸。现在啥都没找到，就不要急着下结论。你们随后再找几天再说吧。明天一早，我再去公社把情况说一下，叫安排广播站广播一个寻人启事，把大家都发动起来，不管是找到活人，退一步，就算是尸首，人多了，找到的希望总归大一些。”
        
        作者：南山顽石
        链接：https://www.jianshu.com/p/fd0903fdd408
        来源：简书
        著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。`,
        good: 0,
        bad: 0,
        comment: 0,
    },
    {
        title: `react【二】`,
        time: `2020.06.21 11:22:13`,
        author: `南山顽石`,
        content: `阵嘻嘻哈哈的说话声，甚是热闹。敲开门时，却见除了红缨外，汪衍华、孙老师也在，每个人手里都拿着一把牌，原来他们在玩弥竹竿。衍举看了衍华一眼，嘿嘿一笑说：“衍华，今儿黑跟你媳妇同房呀？”张红缨胀得满面通红，但知道汪衍举跟谁说话都是这个样子，没轻没重的，她便不好怪病他，只得讪笑着说：“哪呢！等给孙校长把房子收拾好，天已经不早了。所以衍华就不回去了，今儿黑他在孙校长那儿歇。衍举哥也真是的！我跟衍华都是规规矩矩的老实疙瘩子，你把我俩当啥人了呢？”衍举道：“我说得耍呢。”

        　　衍平道：“红缨，我们也是无事不登三宝殿，是有个事想叫你帮忙。”接着便前拉后扯的说了何秀莲叫水吹走了，他们沿河寻尸首，以及汪耀林叫蛇咬了，被送到县医院去了等语，最后方说想请她去耀臣那儿帮忙做饭。听到何秀莲的死讯，红缨、孙老师和王衍华均极为吃惊。红缨道：“早上到河边送孙校长时，还活蹦乱跳的一个人，咋就叫水吹去了呢？秀莲实在是命苦……”说着说着眼睛就湿了，急忙掏出手绢擦了擦眼角。孙老师皱眉沉思片刻说：“俗话说活要见人，死要见尸。现在啥都没找到，就不要急着下结论。你们随后再找几天再说吧。明天一早，我再去公社把情况说一下，叫安排广播站广播一个寻人启事，把大家都发动起来，不管是找到活人，退一步，就算是尸首，人多了，找到的希望总归大一些。”
        
        作者：南山顽石
        链接：https://www.jianshu.com/p/fd0903fdd408
        来源：简书
        著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。`,
        good: 0,
        bad: 0,
        comment: 0,
    },
    {
        title: `react【三】`,
        time: `2020.06.21 11:22:13`,
        author: `南山顽石`,
        content: `窗户传出阵阵嘻嘻哈哈的说话声，甚是热闹。敲开门时，却见除了红缨外，汪衍华、孙老师也在，每个人手里都拿着一把牌，原来他们在玩弥竹竿。衍举看了衍华一眼，嘿嘿一笑说：“衍华，今儿黑跟你媳妇同房呀？”张红缨胀得满面通红，但知道汪衍举跟谁说话都是这个样子，没轻没重的，她便不好怪病他，只得讪笑着说：“哪呢！等给孙校长把房子收拾好，天已经不早了。所以衍华就不回去了，今儿黑他在孙校长那儿歇。衍举哥也真是的！我跟衍华都是规规矩矩的老实疙瘩子，你把我俩当啥人了呢？”衍举道：“我说得耍呢。”

        　　衍平道：“红缨，我们也是无事不登三宝殿，是有个事想叫你帮忙。”接着便前拉后扯的说了何秀莲叫水吹走了，他们沿河寻尸首，以及汪耀林叫蛇咬了，被送到县医院去了等语，最后方说想请她去耀臣那儿帮忙做饭。听到何秀莲的死讯，红缨、孙老师和王衍华均极为吃惊。红缨道：“早上到河边送孙校长时，还活蹦乱跳的一个人，咋就叫水吹去了呢？秀莲实在是命苦……”说着说着眼睛就湿了，急忙掏出手绢擦了擦眼角。孙老师皱眉沉思片刻说：“俗话说活要见人，死要见尸。现在啥都没找到，就不要急着下结论。你们随后再找几天再说吧。明天一早，我再去公社把情况说一下，叫安排广播站广播一个寻人启事，把大家都发动起来，不管是找到活人，退一步，就算是尸首，人多了，找到的希望总归大一些。”
        
        作者：南山顽石
        链接：https://www.jianshu.com/p/fd0903fdd408
        来源：简书
        著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。`,
        good: 0,
        bad: 0,
        comment: 0,
    },
    {
        title: `react【四】`,
        time: `2020.06.21 11:22:13`,
        author: `南山顽石`,
        content: `房子恰好亮着灯，且从窗户传出阵阵嘻嘻哈哈的说话声，甚是热闹。敲开门时，却见除了红缨外，汪衍华、孙老师也在，每个人手里都拿着一把牌，原来他们在玩弥竹竿。衍举看了衍华一眼，嘿嘿一笑说：“衍华，今儿黑跟你媳妇同房呀？”张红缨胀得满面通红，但知道汪衍举跟谁说话都是这个样子，没轻没重的，她便不好怪病他，只得讪笑着说：“哪呢！等给孙校长把房子收拾好，天已经不早了。所以衍华就不回去了，今儿黑他在孙校长那儿歇。衍举哥也真是的！我跟衍华都是规规矩矩的老实疙瘩子，你把我俩当啥人了呢？”衍举道：“我说得耍呢。”

        　　衍平道：“红缨，我们也是无事不登三宝殿，是有个事想叫你帮忙。”接着便前拉后扯的说了何秀莲叫水吹走了，他们沿河寻尸首，以及汪耀林叫蛇咬了，被送到县医院去了等语，最后方说想请她去耀臣那儿帮忙做饭。听到何秀莲的死讯，红缨、孙老师和王衍华均极为吃惊。红缨道：“早上到河边送孙校长时，还活蹦乱跳的一个人，咋就叫水吹去了呢？秀莲实在是命苦……”说着说着眼睛就湿了，急忙掏出手绢擦了擦眼角。孙老师皱眉沉思片刻说：“俗话说活要见人，死要见尸。现在啥都没找到，就不要急着下结论。你们随后再找几天再说吧。明天一早，我再去公社把情况说一下，叫安排广播站广播一个寻人启事，把大家都发动起来，不管是找到活人，退一步，就算是尸首，人多了，找到的希望总归大一些。”
        
        作者：南山顽石
        链接：https://www.jianshu.com/p/fd0903fdd408
        来源：简书
        著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。`,
        good: 0,
        bad: 0,
        comment: 0,
    },
    {
        title: `react【五】`,
        time: `2020.06.21 11:22:13`,
        author: `南山顽石`,
        content: `子恰好亮着灯，且从窗户传出阵阵嘻嘻哈哈的说话声，甚是热闹。敲开门时，却见除了红缨外，汪衍华、孙老师也在，每个人手里都拿着一把牌，原来他们在玩弥竹竿。衍举看了衍华一眼，嘿嘿一笑说：“衍华，今儿黑跟你媳妇同房呀？”张红缨胀得满面通红，但知道汪衍举跟谁说话都是这个样子，没轻没重的，她便不好怪病他，只得讪笑着说：“哪呢！等给孙校长把房子收拾好，天已经不早了。所以衍华就不回去了，今儿黑他在孙校长那儿歇。衍举哥也真是的！我跟衍华都是规规矩矩的老实疙瘩子，你把我俩当啥人了呢？”衍举道：“我说得耍呢。”

        　　衍平道：“红缨，我们也是无事不登三宝殿，是有个事想叫你帮忙。”接着便前拉后扯的说了何秀莲叫水吹走了，他们沿河寻尸首，以及汪耀林叫蛇咬了，被送到县医院去了等语，最后方说想请她去耀臣那儿帮忙做饭。听到何秀莲的死讯，红缨、孙老师和王衍华均极为吃惊。红缨道：“早上到河边送孙校长时，还活蹦乱跳的一个人，咋就叫水吹去了呢？秀莲实在是命苦……”说着说着眼睛就湿了，急忙掏出手绢擦了擦眼角。孙老师皱眉沉思片刻说：“俗话说活要见人，死要见尸。现在啥都没找到，就不要急着下结论。你们随后再找几天再说吧。明天一早，我再去公社把情况说一下，叫安排广播站广播一个寻人启事，把大家都发动起来，不管是找到活人，退一步，就算是尸首，人多了，找到的希望总归大一些。”
        
        作者：南山顽石
        链接：https://www.jianshu.com/p/fd0903fdd408
        来源：简书
        著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。`,
        good: 0,
        bad: 0,
        comment: 0,
    }
]
var vueObjArr = [
    {
        title: `vue【一】`,
        time: `2020.06.21 11:22:13`,
        author: `南山顽石`,
        content: `户传出阵阵嘻嘻哈哈的说话声，甚是热闹。敲开门时，却见除了红缨外，汪衍华、孙老师也在，每个人手里都拿着一把牌，原来他们在玩弥竹竿。衍举看了衍华一眼，嘿嘿一笑说：“衍华，今儿黑跟你媳妇同房呀？”张红缨胀得满面通红，但知道汪衍举跟谁说话都是这个样子，没轻没重的，她便不好怪病他，只得讪笑着说：“哪呢！等给孙校长把房子收拾好，天已经不早了。所以衍华就不回去了，今儿黑他在孙校长那儿歇。衍举哥也真是的！我跟衍华都是规规矩矩的老实疙瘩子，你把我俩当啥人了呢？”衍举道：“我说得耍呢。”

        　　衍平道：“红缨，我们也是无事不登三宝殿，是有个事想叫你帮忙。”接着便前拉后扯的说了何秀莲叫水吹走了，他们沿河寻尸首，以及汪耀林叫蛇咬了，被送到县医院去了等语，最后方说想请她去耀臣那儿帮忙做饭。听到何秀莲的死讯，红缨、孙老师和王衍华均极为吃惊。红缨道：“早上到河边送孙校长时，还活蹦乱跳的一个人，咋就叫水吹去了呢？秀莲实在是命苦……”说着说着眼睛就湿了，急忙掏出手绢擦了擦眼角。孙老师皱眉沉思片刻说：“俗话说活要见人，死要见尸。现在啥都没找到，就不要急着下结论。你们随后再找几天再说吧。明天一早，我再去公社把情况说一下，叫安排广播站广播一个寻人启事，把大家都发动起来，不管是找到活人，退一步，就算是尸首，人多了，找到的希望总归大一些。”
        
        作者：南山顽石
        链接：https://www.jianshu.com/p/fd0903fdd408
        来源：简书
        著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。`,
        good: 0,
        bad: 0,
        comment: 0,
    },
    {
        title: `vue【二】`,
        time: `2020.06.21 11:22:13`,
        author: `南山顽石`,
        content: `户传出阵阵嘻嘻哈哈的说话声，甚是热闹。敲开门时，却见除了红缨外，汪衍华、孙老师也在，每个人手里都拿着一把牌，原来他们在玩弥竹竿。衍举看了衍华一眼，嘿嘿一笑说：“衍华，今儿黑跟你媳妇同房呀？”张红缨胀得满面通红，但知道汪衍举跟谁说话都是这个样子，没轻没重的，她便不好怪病他，只得讪笑着说：“哪呢！等给孙校长把房子收拾好，天已经不早了。所以衍华就不回去了，今儿黑他在孙校长那儿歇。衍举哥也真是的！我跟衍华都是规规矩矩的老实疙瘩子，你把我俩当啥人了呢？”衍举道：“我说得耍呢。”

        　　衍平道：“红缨，我们也是无事不登三宝殿，是有个事想叫你帮忙。”接着便前拉后扯的说了何秀莲叫水吹走了，他们沿河寻尸首，以及汪耀林叫蛇咬了，被送到县医院去了等语，最后方说想请她去耀臣那儿帮忙做饭。听到何秀莲的死讯，红缨、孙老师和王衍华均极为吃惊。红缨道：“早上到河边送孙校长时，还活蹦乱跳的一个人，咋就叫水吹去了呢？秀莲实在是命苦……”说着说着眼睛就湿了，急忙掏出手绢擦了擦眼角。孙老师皱眉沉思片刻说：“俗话说活要见人，死要见尸。现在啥都没找到，就不要急着下结论。你们随后再找几天再说吧。明天一早，我再去公社把情况说一下，叫安排广播站广播一个寻人启事，把大家都发动起来，不管是找到活人，退一步，就算是尸首，人多了，找到的希望总归大一些。”
        
        作者：南山顽石
        链接：https://www.jianshu.com/p/fd0903fdd408
        来源：简书
        著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。`,
        good: 0,
        bad: 0,
        comment: 0,
    },
    {
        title: `vue【三】`,
        time: `2020.06.21 11:22:13`,
        author: `南山顽石`,
        content: `出阵阵嘻嘻哈哈的说话声，甚是热闹。敲开门时，却见除了红缨外，汪衍华、孙老师也在，每个人手里都拿着一把牌，原来他们在玩弥竹竿。衍举看了衍华一眼，嘿嘿一笑说：“衍华，今儿黑跟你媳妇同房呀？”张红缨胀得满面通红，但知道汪衍举跟谁说话都是这个样子，没轻没重的，她便不好怪病他，只得讪笑着说：“哪呢！等给孙校长把房子收拾好，天已经不早了。所以衍华就不回去了，今儿黑他在孙校长那儿歇。衍举哥也真是的！我跟衍华都是规规矩矩的老实疙瘩子，你把我俩当啥人了呢？”衍举道：“我说得耍呢。”

        　　衍平道：“红缨，我们也是无事不登三宝殿，是有个事想叫你帮忙。”接着便前拉后扯的说了何秀莲叫水吹走了，他们沿河寻尸首，以及汪耀林叫蛇咬了，被送到县医院去了等语，最后方说想请她去耀臣那儿帮忙做饭。听到何秀莲的死讯，红缨、孙老师和王衍华均极为吃惊。红缨道：“早上到河边送孙校长时，还活蹦乱跳的一个人，咋就叫水吹去了呢？秀莲实在是命苦……”说着说着眼睛就湿了，急忙掏出手绢擦了擦眼角。孙老师皱眉沉思片刻说：“俗话说活要见人，死要见尸。现在啥都没找到，就不要急着下结论。你们随后再找几天再说吧。明天一早，我再去公社把情况说一下，叫安排广播站广播一个寻人启事，把大家都发动起来，不管是找到活人，退一步，就算是尸首，人多了，找到的希望总归大一些。”
        
        作者：南山顽石
        链接：https://www.jianshu.com/p/fd0903fdd408
        来源：简书
        著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。`,
        good: 0,
        bad: 0,
        comment: 0,
    },
    {
        title: `vue【四】`,
        time: `2020.06.21 11:22:13`,
        author: `南山顽石`,
        content: `且从窗户传出阵阵嘻嘻哈哈的说话声，甚是热闹。敲开门时，却见除了红缨外，汪衍华、孙老师也在，每个人手里都拿着一把牌，原来他们在玩弥竹竿。衍举看了衍华一眼，嘿嘿一笑说：“衍华，今儿黑跟你媳妇同房呀？”张红缨胀得满面通红，但知道汪衍举跟谁说话都是这个样子，没轻没重的，她便不好怪病他，只得讪笑着说：“哪呢！等给孙校长把房子收拾好，天已经不早了。所以衍华就不回去了，今儿黑他在孙校长那儿歇。衍举哥也真是的！我跟衍华都是规规矩矩的老实疙瘩子，你把我俩当啥人了呢？”衍举道：“我说得耍呢。”

        　　衍平道：“红缨，我们也是无事不登三宝殿，是有个事想叫你帮忙。”接着便前拉后扯的说了何秀莲叫水吹走了，他们沿河寻尸首，以及汪耀林叫蛇咬了，被送到县医院去了等语，最后方说想请她去耀臣那儿帮忙做饭。听到何秀莲的死讯，红缨、孙老师和王衍华均极为吃惊。红缨道：“早上到河边送孙校长时，还活蹦乱跳的一个人，咋就叫水吹去了呢？秀莲实在是命苦……”说着说着眼睛就湿了，急忙掏出手绢擦了擦眼角。孙老师皱眉沉思片刻说：“俗话说活要见人，死要见尸。现在啥都没找到，就不要急着下结论。你们随后再找几天再说吧。明天一早，我再去公社把情况说一下，叫安排广播站广播一个寻人启事，把大家都发动起来，不管是找到活人，退一步，就算是尸首，人多了，找到的希望总归大一些。”
        
        作者：南山顽石
        链接：https://www.jianshu.com/p/fd0903fdd408
        来源：简书
        著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。`,
        good: 0,
        bad: 0,
        comment: 0,
    },
    {
        title: `vue【五】`,
        time: `2020.06.21 11:22:13`,
        author: `南山顽石`,
        content: `着灯，且从窗户传出阵阵嘻嘻哈哈的说话声，甚是热闹。敲开门时，却见除了红缨外，汪衍华、孙老师也在，每个人手里都拿着一把牌，原来他们在玩弥竹竿。衍举看了衍华一眼，嘿嘿一笑说：“衍华，今儿黑跟你媳妇同房呀？”张红缨胀得满面通红，但知道汪衍举跟谁说话都是这个样子，没轻没重的，她便不好怪病他，只得讪笑着说：“哪呢！等给孙校长把房子收拾好，天已经不早了。所以衍华就不回去了，今儿黑他在孙校长那儿歇。衍举哥也真是的！我跟衍华都是规规矩矩的老实疙瘩子，你把我俩当啥人了呢？”衍举道：“我说得耍呢。”

        　　衍平道：“红缨，我们也是无事不登三宝殿，是有个事想叫你帮忙。”接着便前拉后扯的说了何秀莲叫水吹走了，他们沿河寻尸首，以及汪耀林叫蛇咬了，被送到县医院去了等语，最后方说想请她去耀臣那儿帮忙做饭。听到何秀莲的死讯，红缨、孙老师和王衍华均极为吃惊。红缨道：“早上到河边送孙校长时，还活蹦乱跳的一个人，咋就叫水吹去了呢？秀莲实在是命苦……”说着说着眼睛就湿了，急忙掏出手绢擦了擦眼角。孙老师皱眉沉思片刻说：“俗话说活要见人，死要见尸。现在啥都没找到，就不要急着下结论。你们随后再找几天再说吧。明天一早，我再去公社把情况说一下，叫安排广播站广播一个寻人启事，把大家都发动起来，不管是找到活人，退一步，就算是尸首，人多了，找到的希望总归大一些。”
        
        作者：南山顽石
        链接：https://www.jianshu.com/p/fd0903fdd408
        来源：简书
        著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。`,
        good: 0,
        bad: 0,
        comment: 0,
    }
]

var authorObj =
    {
        name:'axiaoyua',
        age:'21',
        speciality:[
            '熟练掌握HTML5,CSS3，JavaScript，TypeScript，对ES6新特性有一定的理解',
            '熟悉ajax，jq 。可以通过ajax方式获取和发送数据，并封装过自己的ajax函数',
            '熟悉js事件轮询机制，对宏微任务的运行机制也有自己的理解',
            '了解nodeJS，并有相关项目经验',
            '熟悉react，vue，react-native框架',
            '能熟练使用redux，mobx管理状态',
            '·············'
        ],
        hobby:'read badminton music····'
    }


// Articles.insertManyData(wenZhangObjArr)
// jsArticles.insertManyData(jsObjArr)
// nodeJSArticles.insertManyData(nodejsObjArr)
// httpArticles.insertManyData(httpObjArr)
// ajaxArticles.insertManyData(ajaxObjArr)
// expressArticles.insertManyData(expressObjArr)
// mongodbArticles.insertManyData(mongoObjArr)
// jqueryArticles.insertManyData(jqObjArr)
// reactArticles.insertManyData(reactObjArr)
// vueArticles.insertManyData(vueObjArr)
// authorCenter.insertOneData(authorObj)

module.exports = {
    Articles,
    Comments,
    jsArticles,
    nodeJSArticles,
    httpArticles,
    ajaxArticles,
    expressArticles,
    mongodbArticles,
    jqueryArticles,
    reactArticles,
    vueArticles,
    authorCenter
}