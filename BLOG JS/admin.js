const express=require('express')
const path=require('path')
const router=express.Router()
const { Articles, Comments } = require('./tools/datatest')

const bodyParser = require('body-parser')
router.use(bodyParser.urlencoded({ extended: false }))

// 引入cookie-parser中间件
const cookieParser = require('cookie-parser')
// 初始化中间件
router.use(cookieParser())

// / == /admin。管理员界面接口
// 判断是否登录成功。成功 -> 发送管理员页面，失败 -> 重定向到登录接口
router.get('/',(req,res)=>{
    var {username,password}=req.cookies
    if(username == 'admin' && password == '123456'){
        res.sendFile(path.resolve('./static/html/admin.html'))
    }else{
        res.redirect('/admin/login')
    }
})

// get请求的login接口 /login == /admin/login
// 如果登陆成功,重定向到管理员界面.失败,发送登录界面  sendFile(绝对路径)
router.get('/login',(req,res)=>{
    var {username,password}=req.cookies
    if(username == 'admin' && password == '123456'){
        res.redirect('/admin')
    }else{
        res.sendFile(path.resolve('./static/html/login.html'))
    }
})

// post请求的login接口。设置cookie
router.post('/login',(req,res)=>{
    var {username,password}=req.body
    if(username == 'admin' && password == '123456'){
        // 设置cookie
        res.cookie('username', 'admin')
        res.cookie('password', '123456')

        // 重定向
        res.redirect('/admin')
    }else{
        res.send('请输入正确的账号或密码')
    }
})

// 获取全部评论
router.get('/AllCommands',(req,res)=>{
    Comments.findData({},(err,error,final)=>{
        if(!err && !error){
            // 解决跨域
            res.setHeader('Access-Control-Allow-Origin','*')
            res.send(final)
        }
    })

})

// 改变评论接口。用post请求写。因为前端会传递评论的id过来
// 通过
router.post('/passCommand',(req,res)=>{
    var {id}=req.body
    Comments.updateOneDataById(id,{state:1},(err,error,final)=>{
        if(!err && !error){
            res.send('数据修改成功')
        }
    })
})
// 通过
router.post('/nopassCommand',(req,res)=>{
    var {id}=req.body
    Comments.updateOneDataById(id,{state:2},(err,error,final)=>{
        if(!err && !error){
            res.send('数据修改成功')
        }
    })
})
// 删除
router.post('/deleteCommand',(req,res)=>{
    var {id}=req.body
    Comments.deleteOneDataById(id,(err,error,final)=>{
        if(!err && !error){
            res.send('数据删除成功')
        }
    })
})


// 获取某篇文章对应的评论
router.post('/getOnePageCommand',(req,res)=>{
    var {id}=req.body
    Comments.findData({articleId:id,state:1},(err,error,final)=>{
        if(!err && !error){
            res.send(final)
        }
    })
})

module.exports=router