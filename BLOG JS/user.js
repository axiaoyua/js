const express = require('express')
const path=require('path')
const router = express.Router()

const { 
    Articles, 
    Comments,
    jsArticles,  
    nodeJSArticles,
    httpArticles,
    ajaxArticles,
    expressArticles,
    mongodbArticles,
    jqueryArticles,
    reactArticles,
    vueArticles,
    authorCenter
} = require('./tools/datatest')

const ejs = require('ejs')

const moment = require('moment')

const bodyParser = require('body-parser')
router.use(bodyParser.urlencoded({ extended: false }))

// 主接口。获取全部文章 -> 把数据发送给ejs文件 -> 把ejs处理过后的结果作为响应发送给前端
router.get('/', (req, res) => {
    Articles.findData({}, (err, error, final) => {
        if (!err && !error) {
            ejs.renderFile('./static/ejs/index.ejs', { allArticlesArr: final }, (err1, finalHtml) => {
                if (!err1) {
                    res.send(finalHtml)
                }
            })
        }

    })
})

// 点击某个文章跳转到的接口。
router.get('/page',(req,res)=>{
    var {id}=req.query
    if(req.query.names){
        var {names}=req.query
        eval(`${names}Articles`).findDataById(id,(err,error,final)=>{
            if(!err && !error){
                ejs.renderFile('./static/ejs/page.ejs',{oneArticleObj:final[0],KindName:names},(err1,finalHtml)=>{
                    if(!err1){
                        res.send(finalHtml)
                    }
                })
            }
        })
    }else{
        Articles.findDataById(id,(err,error,final)=>{
            if(!err && !error){
                ejs.renderFile('./static/ejs/page.ejs',{oneArticleObj:final[0],KindName:''},(err1,finalHtml)=>{
                    if(!err1){
                        res.send(finalHtml)
                    }
                })
            }
        })
    }
})

// 添加评论接口。
router.post('/addCommand',(req,res)=>{
    var {kindName,articleId}=req.body
    Comments.insertOneData({
        ...req.body,
        commentTime:moment().format('MMMM Do YYYY, h:mm:ss a'),
        state:0
    },(err,error,final)=>{
        if(!err&&!error){ 
            if (final.acknowledged) {
                if(kindName==''){
                    res.redirect(`/page?id=${articleId}`)
                }else{
                    res.redirect(`/page?id=${articleId}&&names=${kindName}`)
                }
            } else {
                res.send('数据插入失败')
            }
        }  
    })
    
})
// 评论展示的逻辑：state：0，初始状态。1：允许展示：2：不允许展示。删除

// 点击筛选按钮请求的接口
router.get('/filterArticles',(req,res)=>{
    var {names}=req.query
    eval(`${names}Articles`).findData({},(err,error,final)=>{
        res.send(final)
    })
})
//eval（string） =变量名


// 点赞接口
router.post('/good',(req,res)=>{
    var {names,id,number1}=req.body
    eval(`${names}Articles`).updateOneDataById(id,{good:number1},(err,error,final)=>{
        res.send('qqqqqqqqqqqqq')
    })
})
// 踩接口
router.post('/bad',(req,res)=>{
    var {names,id,number1}=req.body
    eval(`${names}Articles`).updateOneDataById(id,{bad:number1},(err,error,final)=>{
        res.send(final)
    })
})


// 作者主页接口
router.get('/authorPage',(req,res)=>{
    authorCenter.findData({},(err,error,final)=>{
        if(!err && !error){
            ejs.renderFile('./static/ejs/author.ejs',{selfObj:final[0]},(err1,finalHtml)=>{
                if(!err1){
                    res.send(finalHtml)
                }
            })
        }
    })
})

// 跳转至发表文章页
router.get('/write',(req,res)=>{
    res.sendFile(path.resolve('./static/html/write.html'))
})

// 发表文章接口
router.post('/addArticle',(req,res)=>{
    Articles.insertOneData({
        ...req.body,
        time:moment().format('MMMM Do YYYY, h:mm:ss a')
    },(err,error,final)=>{
        if(!err && !error){
            res.redirect('/')
        }
    })
})

module.exports = router