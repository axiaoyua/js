const express=require('express')

// 创建服务器
const app=express()

// 分摊路由
app.use('/',require('./user'))
app.use('/admin',require('./admin'))

// 监听端口
app.listen(9999)